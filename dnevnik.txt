S1: 7.10.2015, 11 h, FER 
	Prisutni: Nikola Bukovac, Herman Zvonimir Do�ilovi�, Josipa Kelava, Ivan Krpelnik, Andrea Mati�, Helena Tamburi�, Nikola Vreb�evi�

	 Sa�etak: Upoznavanje �lanova tima, razgovor o predznanju i prethodnim iskustvima. Predlaganje mogu�ih programskih jezika za razvoj aplikacije. Izbor voditelja i imena grupe.

 	Zaklju�ci: Koordinator �e grupu prijaviti pod imenom �espresso�, a �eljeni zadatak je web aplikacija. Za voditelja grupe izabrana je Andrea Mati�.

S2: 21.10.2015. 15:30 h, FER
	Prisutni: Nikola Bukovac, Herman Zvonimir Do�ilovi�, Josipa Kelava, Ivan Krpelnik,  Andrea Mati�, Helena Tamburi�, Nikola Vreb�evi�

	Sa�etak: Zajedni�ko prou�avanje projektnog zadatka, glasanje o programskom  jeziku, odlazak na prve konzultacije kod asistentice i rasprava o nejasnim djelovima zadatka. Dogovor o podijeli poslova.

	Zaklju�ci: Za programski jezik u kojem �emo ostvariti aplikaciju izabran je C#. Poslovi na projektu su okvirno podijeljeni me�u �lanovima kao podgrupama na idu�i na�in (mogu�e manje modulacije kroz nastavak rada):
		Andrea Mati� (voditelj grupe):
		-	Koordinacija rada, pra�enje i pisanje dokumentacije (opis zadatka, dnevnik sastajanja, funkcionalni zahtjevi)
		Helena Tamburi�, Nikola Bukovac:
		-	Baza podataka, pisanje dokumentacije (arhitektura i dizajn sustava)
		Nikola Vreb�evi�, Josipa Kelava:
		-	User-end dizajn i impelentacija, pisanje dokumentacije (ostali zahtjevi, use-case dijagram)
		Ivan Krpelnik, Herman Zvonimir Do�ilovi�:
		-	Razvoj web aplikacije

S3: 29.10.2015., 13 h, FER
	Prisutni: Nikola Vreb�evi�, Andrea Mati�

	Sa�etak: Uz pomo� asistentice Frid analiziran je use case dijagram i raspravljeni   potencijalni problemi sa sekvencijskim dijagramom.

S4:  4.11.2015., 11 h, FER
	Prisutni: Nikola Bukovac,  Josipa Kelava, Ivan Krpelnik,  Andrea Mati�, Helena Tamburi�, Nikola Vreb�evi�

	Sa�etak: Zajedni�ki je analiziran ukupni obavljeni posao, raspravljeni su problemi i nejasno�e oko baze podataka, podijeljen je ostatak dokumentacije koji je potreban ispisati do idu�eg sastanka s asistenticom.

	Zaklju�ak: 
	Poslovi su podijeljeni izme�u �lanova na slijede�i na�in:
	Andrea Mati� (voditelj grupe):
	-	dnevnik sastajanja, funkcionalni zahtjevi
	Helena Tamburi�, Nikola Vreb�evi�, Herman Zvonimir Do�ilovi�:
	-	sekvencijski dijagrami
	Ivan Krpelnik, Josipa Kelava:
	-	ostali zahtjevi, dijagram razreda
	Nikola Bukovac:
	-	dijagram objekta


S5: 9.11.2015., 14 h, FER

   	Prisutni: Nikola Bukovac, Andrea Mati�, Helena Tamburi�, Nikola Vreb�evi�

    Sa�etak:  Uz pomo� asistentice Frid, analiziran je dio dokumentacije napravljen do sad. Nakon njenih par savjeta, raspravljen je na�in izrade sekvencijskih dijagrama i trenutne nejasno�e oko prikaza baze podataka u poglavlju o arhitekturi sustava.

    Zaklju�ak: Svatko nastavlja raditi na svom dijelu zadatka po planu s pro�log sastanka. Unose se odre�ene izmjene u poglavlje 6.1, na opisu baze podataka.

S6: 19.11. 2015., 12:20 h, FER
	Prisutni: Nikola Bukovac, Ivan Krpelnik, Andrea Mati�, Nikola Vreb�evi�

	Sa�etak: S asistenticom Frid raspravljen i proanaliziran dokument verzija 0.42. Asistentica je ukazala na gre�ke i dala savjete za usavr�avanje dokumentacije

	Zaklju�ak: Do 20.11. ispravit �e se pogre�ke i predati verzija 1.0.

s7: 11.1.2016., 10 h, FER
	Prisutni: Nikola Bukovac, Herman Zvonimir Do�ilovi�, Josipa Kelava, Ivan Krpelnik,  Andrea Mati�, Helena Tamburi�, Nikola Vreb�evi�
	
	Sa�etak: Raspravljen je dosada�nji napredak rada na projektu i nejasno�e oko odre�enih dijelova baze podataka. Tamburi�, Vreb�evi�, Mati� i Kelava podijelili su ostatak dokumentacije koju je potrebno napisati do 15.1.

	Zaklju�ak: Rad na projektu ide po planu, nastavlja se po planu.